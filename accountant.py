import sys

accountant = []  # historia
account_sum = 0  # suma wszystkich operacji
warehouse = {}   # magazyn
zmienna = input()

# pobieranie wartosci z pliku  za pomoca 'input()'

while zmienna:
    if zmienna != 'stop':
        accountant.append(zmienna)  # petla, az do 'stop', dodawanie zmiennych
        zmienna = input()           # do listy
    elif zmienna == 'stop':
        i = 0

# jesli 'stop', to obliczanie sumy salda, zakupow i sprzedarzy z pliku
# oraz dodawanie lub odejmowanie towaru z magazynu,
# obsługa bledow

        while i < len(accountant):
            if accountant[i] == 'saldo':
                account_sum += int(accountant[i + 1])
            elif accountant[i] == 'zakup':
                account_sum -= int(accountant[i + 2]) * int(
                    accountant[i + 3])              # odejmowanie z konta
                product = accountant[i + 1]
                price = int(accountant[i + 2])
                quantity = int(accountant[i + 3])
                if account_sum >= price * quantity:  # dodawanie do magazynu
                    if warehouse.get(product):
                        warehouse[product] += quantity
                    else:
                        warehouse[product] = quantity
                else:
                    print("Błąd. Za duża cena")
            elif accountant[i] == 'sprzedaz':
                account_sum += int(accountant[i + 2]) * int(
                    accountant[i + 3])              # dodawanie do konta
                product = accountant[i + 1]
                price = int(accountant[i + 2])
                quantity = int(accountant[i + 3])
                if warehouse.get(product) and warehouse.get(product) \
                        >= quantity:                # odejmowanie z magazynu
                    warehouse[product] -= quantity
                else:
                    print("Blad. Za malo produktow aby zrealizować"
                          " transakcje")
            i += 1
        break
    else:
        zmienna = None  # jesli nie dodano pliku, pominiecie dzialan powyzszych
        break

# wczytywanie ze standardowego wejscia

zmienna2 = (sys.argv[0])
while zmienna2 == (sys.argv[0]):

    # liczenie ile argumentow podano

    arguments = len(sys.argv)
    action = (sys.argv[1])

    # w zaleznosci od wybranej akcji, obsluga bledow oraz obliczanie
    # zmienna *_balance dodaje lub odejmuje wynik do historii

    if action == 'saldo' and arguments == 4:
        if len(sys.argv) < 4 or len(sys.argv) > 4:
            print("Podano zla ilosc argumentow")
        else:
            balance = [action, (sys.argv[2]), (sys.argv[3])]
            accountant += balance               # dodanie wartosci do historii
            account_sum += int((sys.argv[2]))   # i obliczenie sumy konta
            accountant.append('stop')
            i = 0
            while accountant[i] != 'stop':
                print(accountant[i])
                i += 1                          # wpisanie do historii
            print('stop')
            print(account_sum)
    elif action == 'zakup' and arguments == 5:
        if len(sys.argv) < 5 or len(sys.argv) > 5:
            print("Podano zla ilosc argumentow")
        else:
            purchase_balance = account_sum - int((sys.argv[3])) * int(
                (sys.argv[4]))
            if int(sys.argv[3]) <= 0 or int(sys.argv[4]) < 0:
                print("Błąd! Ujemna cena lub ilość sztuk mniejsza"
                      " od zera")
            elif purchase_balance < 0:
                print("Błąd! Saldo jest ujemne")
            else:
                # wpisanie do historii
                purchase = [action, (sys.argv[2]), (sys.argv[3]),
                            (sys.argv[4])]
                accountant += purchase
                # odjecie z konta
                account_sum -= int((sys.argv[3])) * int((sys.argv[4]))
                product = (sys.argv[2])
                price = int((sys.argv[3]))
                quantity = int((sys.argv[4]))
                if account_sum >= price * quantity:
                    account_sum -= price * quantity
                    # wpisanie do magazynu
                    if warehouse.get(product):
                        warehouse[product] += quantity
                    else:
                        warehouse[product] = quantity
                else:
                    print("Błąd. Za duża cena")
                accountant.append('stop')
                i = 0
                while accountant[i] != 'stop':
                    print(accountant[i])               # wpisanie do historii
                    i += 1
                print('stop')
    elif action == 'sprzedaz' and arguments == 5:
        if len(sys.argv) < 5 or len(sys.argv) > 5:
            print("Podano zla ilosc argumentow")
        else:
            sale_balance = account_sum + int((sys.argv[3]))\
                           * int((sys.argv[4]))
            if int(sys.argv[3]) <= 0 or int(sys.argv[4]) < 0:
                print("Błąd! Ujemna cena lub ilość sztuk mniejsza od"
                      " zera")
            elif sale_balance < 0:
                print("Blad! Saldo jest ujemne")
            else:
                sale = [action, (sys.argv[2]), (sys.argv[3]),
                        (sys.argv[4])]
                # dodanie do historii
                accountant += sale
                # dodanie do konta
                account_sum += int((sys.argv[3])) * int((sys.argv[4]))
                product = (sys.argv[2])
                price = int((sys.argv[3]))
                quantity = int((sys.argv[4]))
                if warehouse.get(product) and warehouse.get(product) \
                        >= quantity:
                    warehouse[product] -= quantity  # odjecie z magazynu
                    account_sum += quantity * price
                else:
                    print("Blad. Za malo produktow aby zrealizować"
                          " transakcje")
                accountant.append('stop')
                i = 0
                while accountant[i] != 'stop':
                    print(accountant[i])        # wpisanie do historii
                    i += 1
                print('stop')
    elif action == 'konto':
        i = 0
        while accountant[i] == 'stop':
            if accountant[i] == 'saldo':
                account_sum += int(accountant[i + 1])
            elif accountant[i] == 'zakup':
                account_sum -= int(accountant[i + 2]) * int(
                    accountant[i + 3])
            elif accountant[i] == 'sprzedaz':
                account_sum += int(accountant[i + 2]) * int(accountant[i + 3])
            i += 1
        print(account_sum)      # wyswietlenie obliczonego konta z pliku
    elif action == 'magazyn':
        licznik = 2
        while 2 <= licznik < len(sys.argv):
            print(sys.argv[licznik] + " : " + str(warehouse[sys.argv[licznik]]))
            licznik += 1        # wyswietlenie z magazynu podanych argumentow
    elif action == 'przeglad':
        _from = int(sys.argv[2])
        _to = int(sys.argv[3])          # wyswietlenie przegladu 'od : do'
        print(accountant[_from:_to])
    elif action == 'stop':
        accountant.append('stop')
        i = 0
        while accountant[i] != 'stop':
            print(accountant[i])        # gdy podane 'stop', wyswietlenie
            i += 1                      # pliku historia (czyli in.txt lub
        print('stop')                   # innej nazwy)
    else:
        print('Niedozwolona akcja!!!')
    break
